<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function store(UserStoreRequest $request): JsonResponse
    {
        $user = User::create($request->validated());
        $token = $user->createToken('default');
        return response()->json([
            'status' => 'success',
            'code' => 201,
            'message' => 'User created',
            'token' => $token->plainTextToken
        ], 201);
    }

}
