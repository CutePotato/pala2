<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->getJson('/api/categories');
        $response->assertStatus(200);
    }

    public function test_store()
    {
        $user = User::factory()->create();
        $token = $user->createToken('default')->plainTextToken;
        $this->withToken($token)->postJson('/api/categories', [
            'user_id' => $user->id,
            'title' => 'test',
            'description' => 'test description'
        ]);
        $this->assertDatabaseHas('categories',
            [
                'title' => 'test',
                'description' => 'test description'
            ]
        );
    }

    public function test_show()
    {
        //truncate first, category user_id is 1. artisan migrate:fresh
        $user = User::factory()->has(Category::factory()->count(1))->create();
        $token = $user->createToken('default')->plainTextToken;
        $category = $user->categories()->first();

        $response = $this->withToken($token)->getJson('/api/categories/'.$category->id);
        $response->assertOk();
    }

    public function test_update()
    {
        $user = User::factory()->has(Category::factory()->count(1))->create();
        $token = $user->createToken('default')->plainTextToken;
        $category = $user->categories()->first();

        $response = $this->withToken($token)->putJson('/api/categories/'.$category->id, [
            'title' => 'test',
            'description' => 'test description'
        ]);
        $response->assertOk();
        $this->assertDatabaseHas('categories',
            [
                'title' => 'test',
                'description' => 'test description'
            ]
        );

        $user2 = User::factory()->has(Category::factory()->count(1))->create();
        $category2 = $user2->categories()->first();

        $response2 = $this->withToken($token)->putJson('/api/categories/'.$category2->id, [
            'title' => 'test',
            'description' => 'test description'
        ]);
        $code = $response2->json('code');
        $this->assertTrue($code == 403);
    }

    public function test_destroy()
    {
        $user = User::factory()->has(Category::factory()->count(1))->create();
        $category = $user->categories()->first();
        $category->delete();
        $this->assertDeleted($category);
    }
}
