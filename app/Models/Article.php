<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['autor_id', 'category_id', 'title', 'content', 'published', 'tags', 'published_at'];

    protected $visible = ['id', 'title', 'content', 'published_at', 'published', 'tags', 'category', 'autor'];

    protected $with = ['category', 'autor'];

    public function autor(): BelongsTo
    {
        return $this->belongsTo(Autor::class)->without('articles');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
