<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'articles.*.title' => 'required|string|max:200',
            'articles.*.content' => 'required|string|max:500',
            'articles.*.published' => 'sometimes|nullable|boolean',
            'articles.*.category' => 'nullable|array:title,description',
            'articles.*.author' => 'nullable|array:first_name,last_name',
            'articles.*.tags' => 'nullable|string|max:250'
        ];
    }
}
