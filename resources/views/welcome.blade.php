<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->

        <!-- Styles -->
        <link rel="stylesheet" href="css/app.css">
        <!-- Script -->
        <script src="js/app.js"></script>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                {{--USER--}}
                <div class="text-3xl font-bold my-6">Usuario</div>
                <div>
                    <form id="createUser" onsubmit="storeUser()" action="/api/users" method="POST" class="dark:text-white">
                        <div class="text-2xl font-bold">Crear Usuario</div>
                        <div class="flex flex-col p-4 m-4">
                            <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                <fieldset>
                                    <legend class="my-2">First Name</legend>
                                    <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="first_name" name="first_name" placeholder="First Name" required maxlength="200">
                                </fieldset>
                                <fieldset>
                                    <legend class="my-2">Last Name</legend>
                                    <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="last_name" name="last_name" placeholder="Last Name" required maxlength="200">
                                </fieldset>
                                <fieldset>
                                    <legend class="my-2">Email</legend>
                                    <input type="email" class="p-3 rounded-xl shadow-lg w-full" id="email" name="email" placeholder="Email" required>
                                </fieldset>
                                <fieldset>
                                    <legend class="my-2">Password</legend>
                                    <input type="password" class="p-3 rounded-xl shadow-lg w-full" id="password" name="password" placeholder="Password" required maxlength="200">
                                </fieldset>
                            </div>

                            <div id="userNotification">
                                <fieldset>
                                    <legend>Status</legend>
                                    <div class="font-bold text-sm text-gray-600" id="userStatus"></div>
                                </fieldset>
                                <fieldset>
                                    <legend>Message</legend>
                                    <div class="font-bold text-sm text-gray-600" id="userMessage"></div>
                                </fieldset>
                                <fieldset>
                                    <legend>Token</legend>
                                    <div class="font-bold text-sm text-gray-600" id="userToken"></div>
                                </fieldset>
                            </div>

                            <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                        </div>
                    </form>
                </div>
                {{--CATEGORY--}}
                <div class="text-3xl font-bold my-6">Categoria</div>
                <div class="flex flex-wrap justify-between">
                    <div>
                        <form id="createCategory" action="/api/categories" method="POST" class="dark:text-white" onsubmit="storeCategory()">
                            <div class="text-2xl font-bold">Crear Categoria</div>
                            <div class="flex flex-col p-4 m-4">
                                <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                    <fieldset>
                                        <legend class="my-2">Title</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="categoryTitle" name="categoryTitle" placeholder="Title" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Description</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="categoryDescription" name="categoryDescription" placeholder="Category Description" required maxlength="200">
                                    </fieldset>
                                </div>

                                <div id="categoryNotification">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="font-bold text-sm text-gray-600" id="categoryStatus"></div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Message</legend>
                                        <div class="font-bold text-sm text-gray-600" id="categoryMessage"></div>
                                    </fieldset>
                                </div>

                                <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div>
                        <form id="updateCategory" onsubmit="updateCategory()">
                            @method('put')
                            <div class="text-2xl font-bold">Actualizar Categoria</div>
                            <div class="flex flex-col p-4 m-4">
                                <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                    <fieldset>
                                        <legend class="my-2">Title</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="categoryTitleUpdate" name="categoryTitleUpdate" placeholder="Title" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Description</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="categoryDescriptionUpdate" name="categoryDescriptionUpdate" placeholder="Category Description" required maxlength="200">
                                    </fieldset>
                                </div>

                                <div id="categoryNotificationUpdate">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="font-bold text-sm text-gray-600" id="categoryStatusUpdate"></div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Message</legend>
                                        <div class="font-bold text-sm text-gray-600" id="categoryMessageUpdate"></div>
                                    </fieldset>
                                </div>

                                <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <div class="text-2xl font-bold">Mostrar Categorias</div>
                    <fieldset>
                        <button class="px-3 py-2 rounded-lg shadow-lg bg-indigo-500 text-white my-4" onclick="indexCategories()">Show Categories</button>
                        <div class="font-bold text-sm text-gray-600">
                            <table id="categoryData" class="w-full">
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                    <div id="categoryShowNotification">
                        <fieldset>
                            <legend>Status</legend>
                            <div class="font-bold text-sm text-gray-600" id="categoryShowStatus"></div>
                        </fieldset>
                        <fieldset>
                            <legend>Message</legend>
                            <div class="font-bold text-sm text-gray-600" id="categoryShowMessage"></div>
                        </fieldset>
                    </div>
                </div>
                {{--AUTOR--}}
                <div class="text-3xl font-bold my-6">Autores</div>
                <div class="flex flex-wrap justify-between">
                    <div>
                        <form id="createAuthor" action="/api/autors" method="POST" class="dark:text-white" onsubmit="storeAuthor()">
                            <div class="text-2xl font-bold">Crear Autor</div>
                            <div class="flex flex-col p-4 m-4">
                                <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                    <fieldset>
                                        <legend class="my-2">First Name</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="authorFirstName" name="authorFirstName" placeholder="First Name" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Last Name</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="authorLastName" name="authorLastName" placeholder="Last Name" required maxlength="200">
                                    </fieldset>
                                </div>

                                <div id="authorNotification">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="font-bold text-sm text-gray-600" id="authorStatus"></div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Message</legend>
                                        <div class="font-bold text-sm text-gray-600" id="authorMessage"></div>
                                    </fieldset>
                                </div>

                                <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div>
                        <form id="updateAuthor" onsubmit="updateAuthor()">
                            @method('put')
                            <div class="text-2xl font-bold">Actualizar Autor</div>
                            <div class="flex flex-col p-4 m-4">
                                <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                    <fieldset>
                                        <legend class="my-2">First Name</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="authorFirstNameUpdate" name="authorFirstNameUpdate" placeholder="First Name" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Last Name</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="authorLastNameUpdate" name="categoryDescriptionUpdate" placeholder="Last Name" required maxlength="200">
                                    </fieldset>
                                </div>

                                <div id="authorNotificationUpdate">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="font-bold text-sm text-gray-600" id="authorStatusUpdate"></div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Message</legend>
                                        <div class="font-bold text-sm text-gray-600" id="authorMessageUpdate"></div>
                                    </fieldset>
                                </div>

                                <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <div class="text-2xl font-bold">Mostrar Autores</div>
                    <fieldset>
                        <button class="px-3 py-2 rounded-lg shadow-lg bg-indigo-500 text-white" onclick="indexAuthors()">Show Authors</button>
                        <div class="font-bold text-sm text-gray-600">
                            <ul id="authorData" class="flex flex-wrap justify-between w-full">

                            </ul>
                        </div>
                    </fieldset>
                    <div id="authorShowNotification">
                        <fieldset>
                            <legend>Status</legend>
                            <div class="font-bold text-sm text-gray-600" id="authorShowStatus"></div>
                        </fieldset>
                        <fieldset>
                            <legend>Message</legend>
                            <div class="font-bold text-sm text-gray-600" id="authorShowMessage"></div>
                        </fieldset>
                    </div>
                </div>
                {{--ARTICLE--}}
                <div class="text-3xl font-bold my-6">Article</div>
                <div class="flex flex-wrap justify-between">
                    <div>
                        <form id="createAuthor" action="/api/articles" method="POST" class="dark:text-white" onsubmit="storeArticle()">
                            <div class="text-2xl font-bold">Crear Article</div>
                            <div class="flex flex-col p-4 m-4">
                                <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                    <fieldset>
                                        <legend class="my-2">Title</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleTitle" name="articleTitle" placeholder="Title" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Content</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleContent" name="articleContent" placeholder="Content" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Published</legend>
                                        <input type="checkbox" class="p-3 rounded-xl shadow-lg w-full" id="articlePublished" name="articlePublished" placeholder="Published">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Category Title</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleCategoryTitle" name="articleCategoryTitle" placeholder="Category Title" maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Category Description</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleCategoryDescription" name="articleCategoryDescription" placeholder="Category Description">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Author First Name</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleAuthorFirstName" name="articleAuthorFirstName" placeholder="Author Firstname">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Author Last Name</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleAuthorLastName" name="articleAuthorLastName" placeholder="Author Lastname">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Tags</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleTags" name="articleTags" placeholder="Tags" required>
                                    </fieldset>
                                </div>

                                <div id="articleNotification">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="font-bold text-sm text-gray-600" id="articleStatus"></div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Message</legend>
                                        <div class="font-bold text-sm text-gray-600" id="articleMessage"></div>
                                    </fieldset>
                                </div>

                                <div class="w-full flex justify-between">
                                    <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                                    <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white" onclick="addOtherArticle()">Add to List</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>
                        <form id="updateArticle" onsubmit="updateArticle()">
                            @method('put')
                            <div class="text-2xl font-bold">Actualizar Articulo</div>
                            <div class="flex flex-col p-4 m-4">
                                <div style="min-width: 360px" class="space-y-3 flex flex-col my-3">
                                    <fieldset>
                                        <legend class="my-2">Title</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleTitleUpdate" name="articleTitleUpdate" placeholder="Title" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Content</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleContentUpdate" name="articleContentUpdate" placeholder="Content" required maxlength="200">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Published</legend>
                                        <input type="checkbox" class="p-3 rounded-xl shadow-lg w-full" id="articlePublishedUpdate" name="articlePublishedUpdate" placeholder="Published">
                                    </fieldset>
                                    <fieldset>
                                        <legend class="my-2">Tags</legend>
                                        <input type="text" class="p-3 rounded-xl shadow-lg w-full" id="articleTagsUpdate" name="articleTagsUpdate" placeholder="Tags" required>
                                    </fieldset>
                                </div>

                                <div id="articleNotificationUpdate">
                                    <fieldset>
                                        <legend>Status</legend>
                                        <div class="font-bold text-sm text-gray-600" id="articleStatusUpdate"></div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Message</legend>
                                        <div class="font-bold text-sm text-gray-600" id="articleMessageUpdate"></div>
                                    </fieldset>
                                </div>

                                <button class="rounded-lg shadow-lg px-3 py-2 bg-indigo-500 text-white">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <div class="text-2xl font-bold">Mostrar Articulos</div>
                    <fieldset>
                        <button class="px-3 py-2 rounded-lg shadow-lg bg-indigo-500 text-white" onclick="indexArticles()">Show Articles</button>
                        <div class="font-bold text-sm text-gray-600">
                            <ul id="articleData">

                            </ul>
                        </div>
                    </fieldset>
                    <div id="articleShowNotification">
                        <fieldset>
                            <legend>Status</legend>
                            <div class="font-bold text-sm text-gray-600" id="articleShowStatus"></div>
                        </fieldset>
                        <fieldset>
                            <legend>Message</legend>
                            <div class="font-bold text-sm text-gray-600" id="articleShowMessage"></div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var selectedCategory = null;
            var selectedAuthor = null;
            var selectedArticle = null;
            var articlesList = [];
            //API
            function storeUser(){
                event.preventDefault();
                let firstName = document.getElementById('first_name').value;
                let lastName = document.getElementById('last_name').value;
                let email = document.getElementById('email').value;
                let password = document.getElementById('password').value;
                axios.default.post('/api/users', {
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': email,
                    'password': password
                }).then(response => {
                    console.log(response);
                    let body = response.data;
                    document.getElementById('userStatus').innerHTML = body.status;
                    document.getElementById('userMessage').innerHTML = body.message;
                    document.getElementById('userToken').innerHTML = body.token;
                    localStorage.setItem('token', body.token);
                }).catch(error => {
                    let body = error.response.data;
                    document.getElementById('userStatus').innerHTML = body.status+" "+body.code;
                    document.getElementById('userMessage').innerHTML = body.message;
                });
            }

            function indexCategories(){
                let categoryStatus = document.getElementById('categoryShowStatus');
                let categoryMessage = document.getElementById('categoryShowMessage');
                axios.default.get('/api/categories', {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        categoryStatus.innerHTML = body.status+' '+body.code;
                        categoryMessage.innerHTML = body.message;
                        listCategory(response.data.data);
                    }).catch(error => {
                        let body = error.response.data;
                        categoryStatus.innerHTML = body.status+' '+body.code;
                        categoryMessage.innerHTML = body.message;
                    });
            }
            function showCategory(category){
                event.preventDefault();
                event.stopPropagation();
                let categoryTitle = document.getElementById('categoryTitleUpdate');
                let categoryDescription = document.getElementById('categoryDescriptionUpdate');
                let categoryStatus = document.getElementById('categoryShowStatus');
                let categoryMessage = document.getElementById('categoryShowMessage');
                axios.default.get('/api/categories/'+category, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        console.log(response.data);
                        let data = response.data;
                        let body = data.data;
                        categoryStatus.innerHTML = data.status+' '+data.code;
                        categoryMessage.innerHTML = data.message;
                        categoryTitle.value = body.title;
                        categoryDescription.value = body.description;
                    }).catch(error => {
                        let body = error.response.data;
                        categoryStatus.innerHTML = body.status+' '+body.code;
                        categoryMessage.innerHTML = body.message;
                    });
            }
            function storeCategory(){
                event.preventDefault();
                let title = document.getElementById('categoryTitle').value;
                let description = document.getElementById('categoryDescription').value;
                let categoryStatus = document.getElementById('categoryStatus');
                let categoryMessage = document.getElementById('categoryMessage');
                axios.default.post('/api/categories', {
                    'title': title,
                    'description': description
                }, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        categoryStatus.innerHTML = body.status;
                        categoryMessage.innerHTML = body.message;
                    })
                    .catch(error => {
                        categoryStatus.innerHTML = error.response.data.status;
                        categoryMessage.innerHTML = error.response.data.message;
                    })
                    .finally(() => indexCategories());
            }
            function updateCategory(){
                event.preventDefault();
                event.stopPropagation();
                let categoryTitle = document.getElementById('categoryTitleUpdate');
                let categoryDescription = document.getElementById('categoryDescriptionUpdate');
                let categoryStatus = document.getElementById('categoryStatusUpdate');
                let categoryMessage = document.getElementById('categoryMessageUpdate');
                if(!selectedCategory) return;
                axios.default.put('/api/categories/'+selectedCategory.id, {
                    'title': categoryTitle.value,
                    'description': categoryDescription.value
                }, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        console.log(response.data);
                        let data = response.data;
                        categoryStatus.innerHTML = data.status+' '+data.code;
                        categoryMessage.innerHTML = data.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        categoryStatus.innerHTML = body.status+' '+body.code;
                        categoryMessage.innerHTML = body.message;
                    });
            }
            function deleteCategory(category){
                event.preventDefault();
                event.stopPropagation();
                let categoryStatus = document.getElementById('categoryShowStatus');
                let categoryMessage = document.getElementById('categoryShowMessage');

                axios.default.delete('/api/categories/'+category, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        categoryStatus.innerHTML = body.status+' '+body.code;
                        categoryMessage.innerHTML = body.message;
                        indexCategories();
                    })
                    .catch(error => {
                        let body = error.response.data;
                        categoryStatus.innerHTML = body.status+' '+body.code;
                        categoryMessage.innerHTML = body.message;
                    })
                    .finally(() => indexCategories());
            }

            function indexAuthors(){
                let authorStatus = document.getElementById('authorShowStatus');
                let authorMessage = document.getElementById('authorShowMessage');
                axios.default.get('/api/autors', {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        authorStatus.innerHTML = body.status+' '+body.code;
                        authorMessage.innerHTML = body.message;
                        listAuthor(response.data.data);
                    })
                    .catch(error => {
                        console.log(error);
                        let body = error.response.data;
                        authorStatus.innerHTML = body.status+' '+body.code;
                        authorMessage.innerHTML = body.message;
                    });
            }
            function showAuthor(author){
                event.preventDefault();
                event.stopPropagation();
                let authorFirstName = document.getElementById('authorFirstNameUpdate');
                let authorLastName = document.getElementById('authorLastNameUpdate');
                let authorStatus = document.getElementById('authorShowStatus');
                let authorMessage = document.getElementById('authorShowMessage');
                axios.default.get('/api/autors/'+author, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        console.log(response.data);
                        let data = response.data;
                        let body = data.data;
                        authorStatus.innerHTML = data.status+' '+data.code;
                        authorMessage.innerHTML = data.message;
                        authorFirstName.value = body.first_name;
                        authorLastName.value = body.last_name;
                    }).catch(error => {
                    let body = error.response.data;
                    authorStatus.innerHTML = body.status+' '+body.code;
                    authorMessage.innerHTML = body.message;
                });
            }
            function storeAuthor(){
                event.preventDefault();
                let firstName = document.getElementById('authorFirstName').value;
                let lastName = document.getElementById('authorLastName').value;
                let authorStatus = document.getElementById('authorStatus');
                let authorMessage = document.getElementById('authorMessage');
                axios.default.post('/api/autors', {
                    'first_name': firstName,
                    'last_name': lastName
                }, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        authorStatus.innerHTML = body.status;
                        authorMessage.innerHTML = body.message;
                    })
                    .catch(error => {
                        authorStatus.innerHTML = error.response.data.status;
                        authorMessage.innerHTML = error.response.data.message;
                    })
                    .finally(() => indexAuthors());
            }
            function updateAuthor(){
                event.preventDefault();
                event.stopPropagation();
                if(!selectedAuthor) return;
                let firstName = document.getElementById('authorFirstNameUpdate');
                let lastName = document.getElementById('authorLastNameUpdate');
                let authorStatus = document.getElementById('authorStatusUpdate');
                let authorMessage = document.getElementById('authorMessageUpdate');
                axios.default.put('/api/autors/'+selectedAuthor.id, {
                    'first_name': firstName.value,
                    'last_name': lastName.value
                }, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        console.log(response.data);
                        let data = response.data;
                        authorStatus.innerHTML = data.status+' '+data.code;
                        authorMessage.innerHTML = data.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        authorStatus.innerHTML = body.status+' '+body.code;
                        authorMessage.innerHTML = body.message;
                    });
            }
            function deleteAuthor(author){
                event.preventDefault();
                event.stopPropagation();
                let authorStatus = document.getElementById('authorShowStatus');
                let authorMessage = document.getElementById('authorShowMessage');
                axios.default.delete('/api/autors/'+author, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        authorStatus.innerHTML = body.status+' '+body.code;
                        authorMessage.innerHTML = body.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        authorStatus.innerHTML = body.status+' '+body.code;
                        authorMessage.innerHTML = body.message;
                    })
                    .finally(() => indexAuthors());
            }

            function indexArticles(){
                let articleStatus = document.getElementById('articleShowStatus');
                let articleMessage = document.getElementById('articleShowMessage');
                axios.default.get('/api/articles', {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                        listArticle(response.data.data);
                    })
                    .catch(error => {
                        let body = error.response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    });
            }
            function showArticle(article){
                event.preventDefault();
                event.stopPropagation();
                let articleTitleUpdate = document.getElementById('articleTitleUpdate');
                let articleContentUpdate = document.getElementById('articleContentUpdate');
                let articlePublishedUpdate = document.getElementById('articlePublishedUpdate');
                let articleTagsUpdate = document.getElementById('articleTagsUpdate');
                let articleStatus = document.getElementById('articleShowStatus');
                let articleMessage = document.getElementById('articleShowMessage');
                axios.default.get('/api/articles/'+article, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let base = response.data;
                        let body = base.data;
                        console.log(body);
                        articleTitleUpdate.value = body.title;
                        articleContentUpdate.value = body.content;
                        articlePublishedUpdate.value = !!body.published;
                        articleTagsUpdate.value = body.tags;
                        articleStatus.innerHTML = base.status+' '+base.code;
                        articleMessage.innerHTML = base.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    });
            }
            function storeArticle(){
                event.preventDefault();
                addOtherArticle();
                let articleStatus = document.getElementById('articleStatus');
                let articleMessage = document.getElementById('articleMessage');
                axios.default.post('/api/articles', {
                    'articles' : articlesList
                }, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    })
                    .finally(() => {
                        clearArticleList();
                        indexArticles();
                    });
            }
            function updateArticle(){
                event.preventDefault();
                event.stopPropagation();
                if(!selectedArticle) return;
                let articleStatus = document.getElementById('articleStatusUpdate');
                let articleMessage = document.getElementById('articleMessageUpdate');
                let title = document.getElementById('articleTitleUpdate').value;
                let content = document.getElementById('articleContentUpdate').value;
                let published = document.getElementById('articlePublishedUpdate').value;
                let tags = document.getElementById('articleTagsUpdate').value;
                axios.default.put('/api/articles/'+selectedArticle.id, {
                    'title': title,
                    'content': content,
                    //!!published | will be better
                    'published': parseInt(published),
                    'tags': tags
                }, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    })
                    .finally(() => indexArticles());
            }
            function deleteArticle(article){
                event.preventDefault();
                event.stopPropagation();
                let articleStatus = document.getElementById('articleShowStatus');
                let articleMessage = document.getElementById('articleShowMessage');
                axios.default.delete('/api/articles/'+article, {
                    headers: {
                        'Authorization': 'Bearer '+localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        let body = response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    })
                    .catch(error => {
                        let body = error.response.data;
                        articleStatus.innerHTML = body.status+' '+body.code;
                        articleMessage.innerHTML = body.message;
                    })
                    .finally(() => indexArticles());
            }

            //SUPPORT
            function listCategory(listData){
                let table = document.getElementById('categoryData');
                table.classList.add('text-center');
                table.classList.add('w-full');
                table.innerHTML = `
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                `;
                listData.forEach(data => {
                    let tr = document.createElement('tr');
                    tr.addEventListener('click', () => {
                        event.preventDefault();
                        event.stopPropagation();
                        updateCategoryForm(data);
                        selectedCategory = data;
                        console.log('update category');
                    });
                    tr.classList.add('cursor-pointer');
                    tr.classList.add('px-3');
                    tr.classList.add('py-2');
                    tr.innerHTML = `
                        <td class="text-lg">`+data.title+`</td>
                        <td class="text-lg">`+data.description+`</td>
                    `;

                    let td = document.createElement('td');
                    td.classList.add('text-lg');
                    let btnDelete = deleteBtn();
                    btnDelete.addEventListener('click', () => {
                        event.preventDefault();
                        event.stopPropagation();
                        deleteCategory(data.id);
                        console.log('delete category');
                    });
                    td.appendChild(btnDelete);

                    tr.appendChild(td);
                    table.appendChild(tr);
                });
            }
            function updateCategoryForm(selectedData){
                showCategory(selectedData.id);
            }

            function listAuthor(listData){
                console.log(listData)
                let ul = document.getElementById('authorData');
                ul.innerHTML = "";
                listData.forEach(data => {
                    let li = document.createElement('li');
                    li.addEventListener('click', () => {
                        event.preventDefault();
                        event.stopPropagation();
                        updateAuthorForm(data);
                        selectedAuthor = data;
                        console.log('update category');
                    });
                    li.classList.add('cursor-pointer');
                    li.classList.add('text-sm');
                    li.classList.add('text-gray-600');
                    li.classList.add('px-3');
                    li.classList.add('py-2');
                    li.innerHTML = `
                        <div>First Name: `+data.first_name+`</div>
                        <div>Last Name: `+data.last_name+`</div>
                    `;

                    let btnDelete = deleteBtn();
                    btnDelete.addEventListener('click', () => {
                        event.preventDefault();
                        event.stopPropagation();
                        deleteAuthor(data.id);
                        console.log('delete article');
                    });
                    li.appendChild(btnDelete);

                    let ul2 = document.createElement('ul');
                    data.articles.forEach(article => {
                        let li2 = document.createElement('li');
                        li2.innerHTML = `
                            <div>Title: `+article.title+`</div>
                            <div>Content: `+article.content+`</div>
                            <div>Published at: `+article.published_at+`</div>
                            <div>Published: `+article.published+`</div>
                            <div>Tags: `+article.tags+`</div>
                            <div>Category: `+article.category.title+`</div>
                            <div>Author: `+article.autor.first_name+`</div>
                        `;
                        ul2.appendChild(li2);
                    })
                    li.appendChild(ul2);
                    ul.appendChild(li);
                });
            }
            function updateAuthorForm(selectedData){
                showAuthor(selectedData.id);
            }

            function listArticle(listData){
                console.log(listData)
                let ul = document.getElementById('articleData');
                ul.innerHTML = "";
                listData.forEach(data => {
                    let li = document.createElement('li');
                    li.addEventListener('click', () => {
                        event.preventDefault();
                        event.stopPropagation();
                        updateArticleForm(data);
                        selectedArticle = data;
                        console.log('update article');
                    });
                    li.classList.add('cursor-pointer');
                    li.classList.add('text-sm');
                    li.classList.add('text-gray-600');
                    li.classList.add('px-3');
                    li.classList.add('py-2');
                    li.innerHTML = `
                        <div>Title: `+data.title+`</div>
                        <div>Content: `+data.content+`</div>
                        <div>Published: `+data.published+`</div>
                        <div>Tags: `+data.tags+`</div>
                        <div>Tags: `+data.published_at+`</div>
                        <div>Category: `+data.category.title+`</div>
                        <div>Author: `+data.autor.first_name+`</div>
                    `;

                    let btnDelete = deleteBtn();
                    btnDelete.addEventListener('click', () => {
                        event.preventDefault();
                        event.stopPropagation();
                        deleteArticle(data.id);
                        console.log('delete article');
                    });
                    li.appendChild(btnDelete);
                    ul.appendChild(li);
                });
            }
            function updateArticleForm(selectedData){
                showArticle(selectedData.id);
            }
            function addOtherArticle(){
                event.preventDefault();
                let title = document.getElementById('articleTitle').value;
                let content = document.getElementById('articleContent').value;
                let published = document.getElementById('articlePublished').value;
                let categoryTitle = document.getElementById('articleCategoryTitle').value;
                let categoryDescription = document.getElementById('articleCategoryDescription').value;
                let authorFirstName = document.getElementById('articleAuthorFirstName').value;
                let authorLastName = document.getElementById('articleAuthorLastName').value;
                let tags = document.getElementById('articleTags').value;
                let article = {
                    'title': title,
                    'content': content,
                    'published': !!published,
                    'category': {
                        'title': categoryTitle,
                        'description': categoryDescription
                    },
                    'author': {
                        'first_name': authorFirstName,
                        'last_name': authorLastName
                    },
                    'tags': tags
                }
                articlesList.push(article);
                clearArticleForm();
            }
            function clearArticleForm(){
                document.getElementById('articleTitle').value = '';
                document.getElementById('articleContent').value = '';
                document.getElementById('articlePublished').value = false;
                document.getElementById('articleCategoryTitle').value = '';
                document.getElementById('articleCategoryDescription').value = '';
                document.getElementById('articleAuthorFirstName').value = '';
                document.getElementById('articleAuthorLastName').value = '';
                document.getElementById('articleTags').value = '';
            }
            function clearArticleList(){
                articlesList = [];
            }

            function deleteBtn(){
                let btnDelete = document.createElement('button');
                btnDelete.innerHTML = 'Delete';
                btnDelete.classList.add('rounded-lg');
                btnDelete.classList.add('shadow-lg');
                btnDelete.classList.add('px-3');
                btnDelete.classList.add('py-2');
                btnDelete.classList.add('bg-indigo-500');
                btnDelete.classList.add('text-white');
                return btnDelete;
            }
        </script>
    </body>
</html>
