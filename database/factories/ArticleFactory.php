<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'autor_id' => 1,
            'category_id' => 1,
            'title' => 'title',
            'content' => 'content',
            'published' => 0,
            'tags' => 'sas,tal',
            'published_at' => now()
        ];
    }
}
