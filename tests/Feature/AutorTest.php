<?php

namespace Tests\Feature;

use App\Models\Autor;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AutorTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->getJson('/api/autors');
        $response->assertStatus(200);
    }

    public function test_store()
    {
        $user = User::factory()->create();
        $token = $user->createToken('default')->plainTextToken;
        $this->withToken($token)->postJson('/api/autors', [
            'user_id' => $user->id,
            'first_name' => 'Pepe',
            'last_name' => 'Hernandez'
        ]);
        $this->assertDatabaseHas('autors',
            [
                'first_name' => 'Pepe',
                'last_name' => 'Hernandez'
            ]
        );
    }

    public function test_show()
    {
        $user = User::factory()->has(Autor::factory()->count(1))->create();
        $token = $user->createToken('default')->plainTextToken;
        $author = $user->autors()->first();

        $response = $this->getJson('/api/autors/'.$author->id, [
            'Authorization' => 'Bearer '.$token
        ]);
        $response->assertOk();
    }

    public function test_update()
    {
        $user = User::factory()->has(Autor::factory()->count(1))->create();
        $token = $user->createToken('default')->plainTextToken;
        $author = $user->autors()->first();

        $response = $this->withToken($token)->putJson('/api/autors/'.$author->id, [
            'first_name' => 'Pepe',
            'last_name' => 'Hernandez'
        ]);
        $response->assertOk();
        $this->assertDatabaseHas('autors',
            [
                'first_name' => 'Pepe',
                'last_name' => 'Hernandez'
            ]
        );

        $user2 = User::factory()->has(Autor::factory()->count(1))->create();
        $author2 = $user2->autors()->first();

        $response2 = $this->withToken($token)->putJson('/api/autors/'.$author2->id, [
            'first_name' => 'Pepe',
            'last_name' => 'Hernandez'
        ]);
        $code = $response2->json('code');
        $this->assertTrue($code == 403);
    }

    public function test_destroy()
    {
        $user = User::factory()->has(Autor::factory()->count(1))->create();
        $author = $user->autors()->first();
        $author->delete();
        $this->assertDeleted($author);
    }
}
