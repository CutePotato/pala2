<?php

namespace App\Traits;

use App\Models\Autor;

trait AutorTrait
{
    public function getOrCreateAutor(array $data): Autor
    {
        $author = Autor::where('first_name', $data['first_name'])
            ->where('last_name', $data['last_name'])
            ->first();
        if(isset($author)) return $author;
        return Autor::create($data);
    }
}
