<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_create()
    {
        $response = $this->get('/api/users/create');
        $response->assertStatus(200);
    }

    public function test_user_store()
    {
        $this->postJson('/api/users', [
            'first_name' => 'test',
            'last_name' => 'test_2',
            'email' => 'test@test.es',
            'password' => 'password'
        ]);
        $this->assertDatabaseHas('users', ['email' => 'test@test.es']);
    }
}
