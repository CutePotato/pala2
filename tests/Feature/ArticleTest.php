<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Autor;
use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $user = User::factory()->create();
        $token = $user->createToken('default')->plainTextToken;
        $response = $this->withToken($token)->getJson('/api/articles');
        $response->assertStatus(200);
    }

    public function test_store()
    {
        $user = User::factory()->create();
        $token = $user->createToken('default')->plainTextToken;
        $this->withToken($token)->postJson('/api/articles', [
            'title' => 'title1',
            'content' => 'content1',
            'published' => 0,
            'category' => ['title' => 'sas', 'description' => 'desct'],
            'author' => ['first_name' => 'lul', 'last_name'=> 'pep'],
            'tags' => 'hi,world'
        ]);
        $this->assertDatabaseHas('articles',
            [
                'title' => 'title1',
            ]
        );
    }

    public function test_show()
    {
        $article = Article::factory()->count(1);
        $author = Autor::factory()->has($article)->count(1);
        $category = Category::factory()->count(1);
        $user = User::factory()
            ->has($author)
            ->has($category)
            ->create();

        $art = $user->autors()->first();
        $token = $user->createToken('default')->plainTextToken;
        $response = $this->withToken($token)->getJson('/api/articles/'.$art->id);
        $response->assertOk();
    }

    public function test_update()
    {
        $article = Article::factory()->count(1);
        $author = Autor::factory()->has($article)->count(1);
        $category = Category::factory()->count(1);
        $user = User::factory()
            ->has($author)
            ->has($category)
            ->create();

        $art = $user->autors()->first();
        $token = $user->createToken('default')->plainTextToken;

        $response = $this->withToken($token)->putJson('/api/autors/'.$art->id, [
            'autor_id' => 1,
            'category_id' => 1,
            'title' => 'sas',
            'content' => 'content sas',
            'published' => 0,
            'tags' => 'sas,tal',
            'published_at' => now()
        ]);
        $response->assertOk();
        $this->assertDatabaseHas('autors',
            [
                'title' => 'sas',
            ]
        );
    }

    public function test_destroy()
    {
        $article = Article::factory()->count(1);
        $author = Autor::factory()->has($article)->count(1);
        $category = Category::factory()->count(1);
        $user = User::factory()
            ->has($author)
            ->has($category)
            ->create();

        $art = $user->autors()->first();
        $token = $user->createToken('default')->plainTextToken;
        $this->withToken($token)->deleteJson('/api/autors/'.$art->id);
        $this->assertDeleted($art);
    }
}
