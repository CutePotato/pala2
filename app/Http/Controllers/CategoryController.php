<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryDeleteRequest;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $categories = Category::all();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Categories details',
            'data' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryStoreRequest $request): JsonResponse
    {
        $validData = $request->validated();
        $category = Category::create([
            'user_id' => $request->user()->id,
            'title' => $validData['title'],
            'description' => $validData['description']
        ]);
        return response()->json([
            'status' => 'success',
            'code' => 201,
            'message' => 'Category '.$category->title.' '.'has been created',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Category details',
            'data' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryUpdateRequest $request, Category $category): JsonResponse
    {
        $category->update($request->validated());
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Category '.$category->title.' '.'has been updated',
            'data' => $category
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category, CategoryDeleteRequest $request): JsonResponse
    {
        if($category->articles()->count()>0){
            return response()->json([
                'status' => 'fail',
                'code' => 409,
                'message' => 'Can\'t be deleted since has articles associated yet',
            ], 409);
        }
        $category->delete();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Category '.$category->title.' '.'has been deleted',
        ]);
    }
}
