<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AutorController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/users', [UserController::class, 'store']);

Route::middleware('auth:sanctum')->group(function (){
    Route::apiResources([
        'autors' => AutorController::class,
        'categories' => CategoryController::class,
        'articles' => ArticleController::class,
    ]);
});
