<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleDeleteRequest;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;
use App\Models\Article;
use App\Models\Autor;
use App\Models\Category;
use App\Traits\AutorTrait;
use App\Traits\CategoryTrait;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    use AutorTrait;
    use CategoryTrait;
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $articles = Article::all();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Articles details',
            'data' => $articles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ArticleStoreRequest $request): JsonResponse
    {
        $validData = $request->validated();
        $isOne = count($validData['articles']) == 1;
        try {
            DB::beginTransaction();

            foreach ($validData['articles'] as $article){
                $categoryData = $article['category'];
                $categoryData['user_id'] = $request->user()->id;
                $category = $this->getOrCreateCategory($categoryData);

                $authorData = $article['author'];
                $authorData['user_id'] = $request->user()->id;
                $author = $this->getOrCreateAutor($authorData);

                $request->user()->categories()->save($category);
                $request->user()->autors()->save($author);

                unset($article['category']);
                unset($article['author']);

                $article['autor_id'] = $author->id;
                $article['category_id'] = $category->id;
                if(!isset($article['published']) || $article['published'] == false){
                    $article['published'] = false;
                    $article['published_at'] = null;
                    $art = Article::create($validData);
                }else{
                    $article['published_at'] = now();
                    $art = Article::create($article);
                }
            }
            DB::commit();
        }catch (Exception $exception){
            DB::rollBack();
            return response()->json([
                'status' => 'fail',
                'code' => 400,
                'message' => 'Request fail'
            ], 400);
        }
        return response()->json([
            'status' => 'success',
            'code' => 201,
            'message' => $isOne?'Article '.$art->title.' has been created':'Articles has been created'
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Article details',
            'data' => $article
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ArticleUpdateRequest $request, Article $article): JsonResponse
    {
        $validData = $request->validated();
        if(!isset($validData['published']) || $validData['published'] == false){
            $validData['published'] = false;
            $validData['published_at'] = null;
            $article->update($validData);
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Article '.$article->title.' has been updated'
            ]);
        }
        $validData['published_at'] = now();
        $article->update($validData);
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Article '.$article->title.' has been updated',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ArticleDeleteRequest $request, Article $article): JsonResponse
    {
        $article->delete();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Author '.$article->title.' has been deleted',
        ]);
    }
}
