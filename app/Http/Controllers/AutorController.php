<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorDeleteRequest;
use App\Http\Requests\AuthorStoreRequest;
use App\Http\Requests\AuthorUpdateRequest;
use App\Models\Autor;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;

class AutorController extends Controller
{
    public function index(): JsonResponse
    {
        $authors = Autor::all();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Authors details',
            'data' => $authors
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AuthorStoreRequest $request): JsonResponse
    {
        $validData = $request->validated();
        $author = Autor::create([
            'user_id' => $request->user()->id,
            'first_name' => $validData['first_name'],
            'last_name' => $validData['last_name']
        ]);
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Author '.$author->first_name.' '.$author->last_name.' '.'has been created',
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Autor $autor): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Author details',
            'data' => $autor
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AuthorUpdateRequest $request, Autor $autor): JsonResponse
    {
        $autor->update($request->validated());
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Author '.$autor->first_name.' '.$autor->last_name.' '.'has been updated',
            'data' => $autor
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Autor $autor, AuthorDeleteRequest $request): JsonResponse
    {
        $autor->delete();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Author '.$autor->first_name.' '.$autor->last_name.' '.'has been deleted',
        ]);
    }
}
