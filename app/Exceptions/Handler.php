<?php

namespace App\Exceptions;

use Facade\FlareClient\Http\Exceptions\InvalidData;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof QueryException || $e instanceof BadRequestException){
            return response()->json([
                'status' => 'error',
                'code' => 400,
                'message' => 'Bad request'
            ], 400);
        }
        if($e instanceof NotFoundHttpException){
            return response()->json([
                'status' => 'error',
                'code' => 404,
                'message' => 'Not Found'
            ], 404);
        }
        if ($e instanceof AuthorizationException){
            return response()->json([
                'status' => 'error',
                'code' => 403,
                'message' => $e->getMessage(),
            ], 403);
        }
        if ($e instanceof ValidationException){
            return response()->json([
                'status' => 'error',
                'code' => 422,
                'message' => $e->getMessage()
            ], 422);
        }
        return response()->json([
            'status' => 'error',
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
        ], 500);
    }
}
