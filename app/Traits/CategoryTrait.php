<?php

namespace App\Traits;

use App\Models\Category;

trait CategoryTrait
{
    public function getOrCreateCategory(array $data): Category
    {
        $category = Category::where('title', $data['title'])->first();
        if(isset($category)) return $category;
        return Category::create($data);
    }
}
